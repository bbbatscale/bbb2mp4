# BBB2mp4

This is an unpolished solution we (Hochschule Darmstadt) use to process our recordings, convert them to mp4, and trigger
the recording webhook of BBB@Scale. This will be reworked in the future, see bbbatscale/bbbatscale#80. To convert the
recordings to mp4 we used a modified version of [bbb-recorder](https://github.com/jibon57/bbb-recorder).

## Prerequisites

Count | Name | Description
--- | --- | ---
1..N | meeting-instance | instances to host meetings
1 | processing-instance | instance which does the conversion to mp4
1..N | playback-instance | instances to playback the processed meetings (can be the same as the meeting instances and/or the processing instance)
1 | | shared filesystem mounted at `/var/bigbluebutton/recording/raw` on meeting and process instances
1 | | shared filesystem mounted at `/var/bigbluebutton/recording/status/sanity` on meeting and process instances
1 | | shared filesystem mounted at `/var/bigbluebutton/published` on process and playback instances

```mermaid
graph TD
  meetingInstance[meeting-instance];
  processingInstance[processing-instance];
  playbackInstance[playback-instance];
  sharedFSRaw[("/var/bigbluebutton/recording/raw<br>/var/bigbluebutton/recording/status/sanity")];
  sharedFSPublished[(/var/bigbluebutton/published)];
  meetingInstance --- sharedFSRaw;
  processingInstance --- sharedFSRaw;
  processingInstance --- sharedFSPublished;
  playbackInstance --- sharedFSPublished
```

## Setup

### Meeting Instances

1. copy the
   [bbb-record-core.target override file](meeting-instance/etc/systemd/system/bbb-record-core.target.d/10-override.conf)
   to `/etc/systemd/system/bbb-record-core.target.d/10-override.conf`
2. restart the instances

### Processing Instances

1. copy the
   [bbb-rap-webhook-worker.service file](processing-instance/etc/systemd/system/bbb-rap-webhook-worker.service)
   to `/etc/systemd/system/bbb-rap-webhook-worker.service`
2. enable the service:
   ```shell
   systemctl enable bbb-rap-webhook-worker.service
   ```
3. copy the [bbb recorder scripts](processing-instance/usr/local/bigbluebutton/core/scripts/post_publish/bbb2mp4)
   to `/usr/local/bigbluebutton/core/scripts/post_publish/bbb2mp4`
4. copy the [bbb2mp4 script](processing-instance/usr/local/bigbluebutton/core/scripts/post_publish/bbb2mp4.rb)
   to `/usr/local/bigbluebutton/core/scripts/post_publish/bbb2mp4.rb`
5. copy the [webhook worker script](processing-instance/usr/local/bigbluebutton/core/scripts/rap-webhook-worker.py)
   to `/usr/local/bigbluebutton/core/scripts/rap-webhook-worker.py`
6. create an env file at `/usr/local/bigbluebutton/core/scripts/rap-webhook-worker.env` like the following:
   ```dotenv
   BBBATSCALE_URL=https://bbbatscale.example.org/
   RECORDINGS_SECRET=DUMMY_SECRET
   ```
7. install Google-Chrome, ffmpeg, xvfb, python3, npm, and the node dependencies:
   ```shell
   wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
   apt install ./google-chrome-stable_current_amd64.deb ffmpeg xvfb python3 npm
   rm google-chrome-stable_current_amd64.deb
   cd /usr/local/bigbluebutton/core/scripts/post_publish/bbb2mp4
   npm install
   ```
8. restart the instance
