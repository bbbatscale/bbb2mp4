let recorder = null;
let filename = null;

chrome.runtime.onConnect.addListener(port => {
    port.onMessage.addListener(msg => {
        switch (msg.type) {
            case "LOG":
                break;

            case "SET_EXPORT_PATH":
                filename = msg.filename;
                break;

            case "REC_STOP":
                recorder.stop();
                break;

            case "REC_START":
                recorder.start();
                break;

            case "REC_CLIENT_PLAY":
                if (recorder) {
                    return;
                }
                const tab = port.sender.tab;
                tab.url = msg.data.url;

                chrome.desktopCapture.chooseDesktopMedia(["tab", "audio"], streamId => {
                    // Get the stream
                    navigator.webkitGetUserMedia({
                        audio: {
                            mandatory: {
                                chromeMediaSource: "system"
                            }
                        },
                        video: {
                            mandatory: {
                                chromeMediaSource: "desktop",
                                chromeMediaSourceId: streamId,
                                minWidth: msg.data.width,
                                maxWidth: msg.data.width,
                                minHeight: msg.data.height,
                                maxHeight: msg.data.height,
                                minFrameRate: msg.data.frameRate,
                                maxFrameRate: msg.data.frameRate
                            }
                        }
                    }, stream => {
                        let chunks = [];
                        recorder = new MediaRecorder(stream, {
                            videoBitsPerSecond: 2500000,
                            ignoreMutedMedia: true,
                            mimeType: "video/webm;codecs=h264"
                        });
                        recorder.ondataavailable = function (event) {
                            if (event.data.size > 0) {
                                chunks.push(event.data);
                            }
                        };

                        recorder.onstop = function () {
                            const superBuffer = new Blob(chunks, {
                                type: "video/webm"
                            });

                            chrome.downloads.download({
                                url: URL.createObjectURL(superBuffer),
                                conflictAction: "overwrite",
                                filename: filename
                            }, downloadId => {
                                if (downloadId === undefined) {
                                    port.postMessage({
                                        type: "LOG",
                                        message: "Unable to save the recording:\n",
                                        optionalParams: JSON.stringify(chrome.runtime.lastError)
                                    })
                                    port.postMessage({downloadError: true});
                                }
                            });
                        }

                    }, error => port.postMessage({
                        type: "LOG",
                        message: "Unable to get user media",
                        optionalParams: JSON.stringify(error)
                    }));
                })
                break;
            default:
                port.postMessage({type: "LOG", message: "Unrecognized message", optionalParams: JSON.stringify(msg)});
                break;
        }
    })

    chrome.downloads.onChanged.addListener(delta => {
        if (delta.state && delta.state.current === "complete") {
            try {
                port.postMessage({downloadComplete: true});
            } catch (e) {
            }
        }
    });
})
