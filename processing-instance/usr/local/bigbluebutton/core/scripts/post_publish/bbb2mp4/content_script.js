const width = 1920;
const height = 1080;
const frameRate = 30;

window.onload = () => {
    if (window.recorderInjected) {
        return;
    }
    Object.defineProperty(window, "recorderInjected", {value: true, writable: false});

    // Setup message passing
    const port = chrome.runtime.connect(chrome.runtime.id);
    port.onMessage.addListener(msg => window.postMessage(msg, "*"));
    window.addEventListener("message", event => {
        // Relay client messages
        if (event.source === window && event.data.type) {
            port.postMessage(event.data);
        }
        if (event.data.downloadComplete || event.data.downloadError) {
            const classList = document.querySelector("html").classList;
            classList.add("downloadComplete");
            if (event.data.downloadError) {
                classList.add("downloadError");
            }
        }
        if (event.data.type === "LOG") {
            console.log(event.data.message, event.data.optionalParams);
        }
    })

    document.title = "bbb2mp4"
    window.postMessage({
        type: "REC_CLIENT_PLAY",
        data: {url: window.location.origin, width: width, height: height, frameRate: frameRate}
    }, "*");
}
