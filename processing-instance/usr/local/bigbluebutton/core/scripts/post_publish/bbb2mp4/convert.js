const puppeteer = require("puppeteer");
const exec = require("child_process").exec;
const fs = require("fs");
const os = require("os");
const xmlConverter = require("xml-js");
const homedir = os.homedir();

const width = 1920;
const height = 1080;
const options = {
    executablePath: "/usr/bin/google-chrome",
    headless: false,
    args: [
        "--enable-usermedia-screen-capturing",
        "--allow-http-screen-capture",
        "--auto-select-desktop-capture-source=bbb2mp4",
        "--load-extension=" + __dirname,
        "--disable-extensions-except=" + __dirname,
        "--disable-infobars",
        "--no-sandbox",
        "--shm-size=1gb",
        "--disable-dev-shm-usage",
        `--window-size=${width},${height + 165}`, // MagicNumber: removes the black bars (obtained by trial and error)
    ],
};


const xvfb = new (require("xvfb"))({
    silent: true,
    xvfb_args: ["-screen", "0", `${width}x${height}x24`, "-ac", "-nolisten", "tcp", "-dpi", "96", "+extension", "RANDR"]
});

async function main() {
    try {
        const meetingId = process.argv[2];

        if (!meetingId) {
            console.log("No meeting Id has been supplied");
            return false;
        }

        const metadata = xmlConverter.xml2js(fs.readFileSync("/var/bigbluebutton/published/presentation/" + meetingId + "/metadata.xml", "utf8"), {compact: true});
        const duration = Number(metadata["recording"]["playback"]["duration"]["_text"]);
        const url = metadata["recording"]["playback"]["link"]["_text"];

        try {
            xvfb.startSync();

            const browser = await puppeteer.launch(options);
            const page = await browser.newPage();

            page.on("console", msg => {
                console.log("PAGE LOG:", msg.text());
            });

            await page._client.send("Emulation.clearDeviceMetricsOverride");
            await page.goto(url, {waitUntil: "networkidle2"});
            await page.setBypassCSP(true);

            await page.waitForSelector("button[class=acorn-play-button]");
            await page.$eval("#navbar", element => element.style.display = "none");
            await page.$eval("#copyright", element => element.style.display = "none");
            await page.$eval(".acorn-controls", element => element.style.display = "none");
            await page.click("video[id=video]", {waitUntil: "domcontentloaded"});

            console.log("Start recording");
            await page.evaluate(() => {
                window.postMessage({type: "REC_START"}, "*");
            })

            // Perform any actions that have to be captured in the exported video
            await page.waitFor(duration);

            await page.evaluate(filename => {
                window.postMessage({type: "SET_EXPORT_PATH", filename: filename}, "*");
                window.postMessage({type: "REC_STOP"}, "*");
            }, meetingId + ".webm")
            console.log("Recording completed");

            // Wait for download of webm to complete
            console.log("Saving recording");
            await page.waitForSelector("html.downloadComplete", {timeout: 0});
            if (await page.$("html.downloadError") !== null) {
                console.log("An error occurred while saving the record");
                return false;
            }
            console.log("Saved recording");
            await page.close();
            await browser.close();

            convertAndCopy(meetingId)

            fs.createWriteStream("/var/bigbluebutton/recording/status/webhook/" + meetingId + ".awaiting").close()
        } catch (error) {
            console.log("An unexpected error occurred:\n", error);
            return false;
        } finally {
            xvfb.stopSync();
        }
    } catch (error) {
        console.log("An unexpected error occurred:\n", error);
        return false;
    }
    return true
}

function convertAndCopy(meetingId) {
    const webmFile = homedir + "/Downloads/" + meetingId + ".webm";
    const mp4File = "/var/bigbluebutton/published/presentation/" + meetingId + "/compact.mp4";

    const cmd = "ffmpeg -y -i '" + webmFile + "' -c:v libx264 -preset veryfast -movflags faststart -profile:v high -level 4.2 -max_muxing_queue_size 9999 -vf mpdecimate -vsync vfr '" + mp4File + "'";

    console.log("Converting recording");
    exec(cmd, function (error) {
        if (error) {
            console.log("An error occurred while converting the webm file:\n", error);
        } else {
            try {
                fs.unlinkSync(webmFile);
            } catch (error) {
                console.log("An error occurred while deleting the webm file:\n", error);
            }
            console.log("Conversion completed");
        }
    });
}

main().then(result => {
    if (result === false) {
        console.log("Conversion failed")
    }
});
