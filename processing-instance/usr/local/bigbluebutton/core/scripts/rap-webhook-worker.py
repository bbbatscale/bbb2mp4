#!/usr/bin/env python3

import glob
import os
from xml.dom.minidom import parse

import requests
from requests.exceptions import RequestException


def status_file(meeting_id: str, status: str) -> str:
    return f"/var/bigbluebutton/recording/status/webhook/{meeting_id}.{status}"


def main():
    bbbatscale_url = os.environ["BBBATSCALE_URL"]
    if not bbbatscale_url.endswith("/"):
        bbbatscale_url += "/"

    for awaiting in glob.iglob(status_file("*", "awaiting")):
        meeting_id = os.path.splitext(os.path.basename(awaiting))[0]
        tree = parse(f"/var/bigbluebutton/published/presentation/{meeting_id}/metadata.xml")
        recording = tree.documentElement
        meta = recording.getElementsByTagName("meta")[0]
        rooms_meeting_id = meta.getElementsByTagName("roomsmeetingid")[0].childNodes[0].data

        print(f"Working on {meeting_id}")

        try:
            response = requests.post(f"{bbbatscale_url}core/api/recording/postporocessing/done/",
                                     json={
                                         "token": os.environ["RECORDINGS_SECRET"],
                                         "rooms_meeting_id": rooms_meeting_id,
                                         "bbb_meeting_id": meeting_id
                                     }, timeout=2)
            if response.status_code == 200:  # HTTP-Response: OK
                os.renames(awaiting, status_file(meeting_id, "done"))
            elif response.status_code == 400:  # HTTP-Response: Bad Request
                os.renames(awaiting, status_file(meeting_id, "fail"))
            elif response.status_code == 401:  # HTTP-Response: Unauthorized
                print("Failed to connect to webhook, caused by an unauthorized access. The token should be checked.")
            else:
                print(f"Unknown http response code {response.status_code}")
            response.close()
        except RequestException as exception:
            print(f"Failed to connect to webhook [{type(exception).__name__}]: {exception.response}")


if __name__ == "__main__":
    main()
